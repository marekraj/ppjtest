import glob
import sys
import os
import re
#from subprocess import Popen, PIPE, STDOUT, TimeoutExpired
import subprocess
from time import time, sleep

# Extensions of expected test results
o_exts = ['b','iz', 'out', 'o']

# Preparing test file paths
test_paths = glob.glob(os.path.join(os.path.abspath(sys.argv[1]), '**'), recursive=True)
test_files = list(filter(os.path.isfile, test_paths))
prefixes = set(map(lambda p: os.path.splitext(p)[0], test_files))
grouped_test_files = []
for prefix in prefixes:
    grouped_test_files.append([prefix] + sorted(filter(lambda f: f.startswith(prefix), test_files)))

# Running tests
print('--- BEGIN TESTS ---')
for num, group in enumerate(grouped_test_files):
    prefix = group[0]
    with open(list(filter(lambda p: re.match('[^.]*\.({})'.format('|'.join(o_exts)), p), group))[0], 'r') as result:
        out = result.read().strip()
    if num:
        print('---')
    # Command to execute
    cmd = sys.argv[2:]
    for j in range(len(cmd)):
        cmd[j] = cmd[j].replace('%TP%', prefix)
    print('Running command: ' + ' '.join(cmd))
    t1 = time()
    actual = subprocess.check_output(' '.join(cmd), shell=True, universal_newlines=True).strip()
    time_took = time() - t1
    print(f'Time: { time_took : .3f}s')
    print(f'Result: {"OK" if out == actual else "ERROR"}')
print('--- END TESTS ---')

